from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def index():
    font_size = os.getenv("FONT_SIZE") or "150px"
    return f'<!-- Up (&#x2705;) Down (&#x274C;) --><p style="font-size: {font_size};"> &#x274C; </p>'


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
