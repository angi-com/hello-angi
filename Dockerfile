FROM python:3-alpine

COPY ./requirements.txt /tmp

RUN pip install -r /tmp/requirements.txt

COPY ./app /app

CMD ["python", "app/app.py"]
